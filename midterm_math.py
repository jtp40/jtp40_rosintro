import sympy as sp
from sympy import pprint


# symbolic vars for robot joint values at key boses
q1, q2, q3, q4, q5, q6 = sp.symbols('q1 q2 q3 q4 q5 q6')

# DH parameters for the UR5e
DH = sp.Matrix([
    [q1, 0.1625, 0, sp.pi/2],
    [q2, 0, -0.425, 0],
    [q3, 0, -0.3922, 0],
    [q4, 0.133, 0, sp.pi/2],
    [q5, 0.0997, 0, -sp.pi/2],
    [q6, 0.0996, 0, 0]
])

# creating A matrix templates for each joint
def compute_A_matrices():
    A_matrix_list = []

    # compute A matrices for each joint
    for i in range(6):
        q, d, a, alpha = DH[i,:]
        A = sp.Matrix([
            [sp.cos(q), -sp.cos(alpha)*sp.sin(q), sp.sin(alpha)*sp.sin(q), a*sp.cos(q)],
            [sp.sin(q), sp.cos(alpha)*sp.cos(q), -sp.sin(alpha)*sp.cos(q), a*sp.sin(q)],
            [0, sp.sin(alpha), sp.cos(alpha), d],
            [0, 0, 0, 1]
        ])
        A_matrix_list.append(A)
    
    # Print the A matrices
    print("TEMPLATE A MATRICES")
    i = 0
    for mat in A_matrix_list:
        print("A_",i)
        pprint(mat.evalf(5))
        print()
        i += 1

    return A_matrix_list

# Creating A matrix templates for each joint
def compute_A_matrices_for_pose(key_pose_joints):
    # compute A matrices for each pose and each joint
    for pose_idx, pose in enumerate(key_pose_joints):
        A_mat_for_pose = []
        print("POSE", pose_idx + 1, "A MATRICES")
        
        # each pose has 6 joints and therefore 6 A matrices
        for i in range(6):
            q, d, a, alpha = DH[i,:]
            A = sp.Matrix([
                [sp.cos(q), -sp.cos(alpha)*sp.sin(q), sp.sin(alpha)*sp.sin(q), a*sp.cos(q)],
                [sp.sin(q), sp.cos(alpha)*sp.cos(q), -sp.sin(alpha)*sp.cos(q), a*sp.sin(q)],
                [0, sp.sin(alpha), sp.cos(alpha), d],
                [0, 0, 0, 1]
            ])
            
            # substitute the joint values for the current pose
            A_sub = A.subs([(q1, pose[0]), (q2, pose[1]), (q3, pose[2]), (q4, pose[3]), (q5, pose[4]), (q6, pose[5])])
            A_mat_for_pose.append(A_sub)
            
        # Print the A matrices for the current pose
        for i, mat in enumerate(A_mat_for_pose):
            print("A_" + str(i))
            pprint(mat.evalf(5))
            print()

# compute template T matrix for each joint
def compute_T_matrix_template(A_matrix_list):
    # compute T matrix template by multiplying the A matrices templates
    T_matrix = A_matrix_list[0]
    for i in range(1, 6):
        T_matrix *= A_matrix_list[i]

    # print the T matrix template
    print("TEMPLATE T MATRIX: ")
    pprint(T_matrix)
    print()

    return T_matrix

def compute_T_matrices(A_matrix_list, key_poses):
    # compute T matrices for each pose
    T_matrices_for_all_poses = []
    for pose in key_poses:
        T_matrices = []
        T = sp.eye(4)
        # multiply the A matrices for each joint and since there are 6 joints, 6 multiplications
        for i, A in enumerate(A_matrix_list):
            T *= A.subs([(q1, pose[0]), (q2, pose[1]), (q3, pose[2]), (q4, pose[3]), (q5, pose[4]), (q6, pose[5])])
            T_matrices.append(T.copy())
        T_matrices_for_all_poses.append(T_matrices)

    # print resulting T matrices for each pose
    pose_index = 1
    for matrices in T_matrices_for_all_poses:
        print("POSE", pose_index, "T Matrix")
        pose_index += 1
        print("T_0_5 =")
        pprint(T.evalf(5))
        print()
    
    return T_matrices_for_all_poses

def compute_jacobians(T_matrices_for_all_poses):
    # compute jacobian for each pose
    for pose_idx, T_matrix_for_pose in enumerate(T_matrices_for_all_poses):
        # create lists for the z and o vectors for each joint
        z_base = [sp.Matrix([0, 0, 1])]
        o_base = [sp.Matrix([0, 0, 0])]

        # compute z and o vectors for each joint
        for T in T_matrix_for_pose:
            z_base.append(T[:3, 2])
            o_base.append(T[:3, 3])

        top_of_jacobian = []
        bottom_of_jacobian = []

        # compute the top and bottom of the jacobian for each joint
        for i in range(6):
            top_of_jacobian.append(z_base[i].cross(o_base[-1] - o_base[i]))
            bottom_of_jacobian.append(z_base[i])

        # create the jacobian matrix
        jacobian = top_of_jacobian[0].col_join(bottom_of_jacobian[0])
        for i in range(1, 6):
            jacobian = jacobian.row_join(top_of_jacobian[i].col_join(bottom_of_jacobian[i]))

        # print the jacobian matrix
        print("POSE", pose_idx + 1, "JACOBIAN")
        pprint(jacobian.evalf(5))
        print()

if __name__ == "__main__":
    # joint values for each key pose
    key_pose_joints = [
        [-0.00010224658785240592, -1.5723885099956867, -1.578512799982379, -1.5732253171273536, 0.00019737094879168637, 1.535549109021872e-05],
        [0.10003066309367004, -0.8677280716505011, -0.7501939411073719, -1.5698452365154338, -1.5699008512524637, 1.1554659895551822e-05],
        [0.06322590870282951, -0.1315598331306731, -1.8712166804464818, -1.1748466623345397, -1.533590067047374, -0.0005470462160115375]
    ]

    # create A matrix templates for each joint and tehn the actual A matrices
    a_matrices = compute_A_matrices()
    compute_A_matrices_for_pose(key_pose_joints)

    # create T matrix template and then the actual T matrices for each pose
    T_template = compute_T_matrix_template(a_matrices)
    T_matrices_for_all_poses = compute_T_matrices(a_matrices, key_pose_joints)

    # compute jacobian for each pose
    compute_jacobians(T_matrices_for_all_poses)