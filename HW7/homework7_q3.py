import sympy as sp

# parameters in DH table
q1, q2, a1, a2 = sp.symbols('q1 q2 a1 a2')

# DH table
Jacobian = sp.Matrix([
    [-a1*sp.sin(q1) - a2*sp.sin(q1 + q2), -a2*sp.sin(q1 + q2)],
    [a1*sp.cos(q1) + a2*sp.cos(q1 + q2), a2*sp.cos(q1 + q2)],
    [0, 0],
    [0, 0],
    [0, 0],
    [1, 1]
])

# print Jacobian
sp.pprint(Jacobian)

# get 2x2 Jacobian
Jacobian_2x2 = Jacobian[0:2, :]

# print 2x2 Jacobian
det_Jacobian_2x2 = Jacobian_2x2.det()

# find singularities and print them
singularities = sp.solve(sp.simplify(det_Jacobian_2x2), [q1, q2])

print("Determinant of the 2x2 matrix:", det_Jacobian_2x2)
print("Simplified determinant of the 2x2 matrix:", sp.simplify(det_Jacobian_2x2))
print("Singularities:")
for singularity in singularities:
    print(singularity)

