import sympy as sp

# Define symbols
q1, q2, d1, d3, a1, a2 = sp.symbols('q1 q2 d1 d3 a1 a2')

# Transformation Matrices
# Compute the transformation matrix from joint i-1 to joint i
def T(i, theta, d, a, alpha):
    return sp.Matrix([
        [sp.cos(theta), -sp.sin(theta), 0, a],
        [sp.sin(theta)*sp.cos(alpha), sp.cos(theta)*sp.cos(alpha), -sp.sin(alpha), -sp.sin(alpha)*d],
        [sp.sin(theta)*sp.sin(alpha), sp.cos(theta)*sp.sin(alpha), sp.cos(alpha), sp.cos(alpha)*d],
        [0, 0, 0, 1]
    ])

# Homogeneous transformation matrices A_i from frame {i-1} to {i}
A1 = T(1, q1, d1, a1, 0)
A2 = T(2, q2, 0, a2, sp.pi)
A3 = T(3, 0, d3, 0, 0)  # This matrix is constant, no dependence on any q_i

# The end-effector frame {4} in terms of the base frame {0}
T04 = A1 * A2 * A3

# Position of the end-effector
O4 = T04[:3, 3]

# Rotation matrix of the end-effector
R04 = T04[:3, :3]

# Linear Jacobian
Jv1 = sp.diff(O4, q1)
Jv2 = sp.diff(O4, q2)
Jv3 = sp.Matrix([0, 0, 1])  # Prismatic joint

Jv = sp.Matrix.hstack(Jv1, Jv2, Jv3)

# Angular Jacobian
z0 = sp.Matrix([0, 0, 1])
z1 = A1[:3, 2]  # Extract the z-axis vector from A1

Jw1 = z0
Jw2 = z1
Jw3 = sp.Matrix([0, 0, 0])  # Prismatic joint has no contribution to angular velocity

Jw = sp.Matrix.hstack(Jw1, Jw2, Jw3)

# Complete Jacobian
J = sp.Matrix.vstack(Jv, Jw)

# Print the Jacobian Matrix
sp.pprint(J)