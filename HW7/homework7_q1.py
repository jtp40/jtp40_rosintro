import sympy as sp

# parameters in DH table
q1, l1, d2, d3 = sp.symbols('q1 l1 d2 d3')

# DH table
DH = sp.Matrix([
    [q1, l1, 0, 0],
    [0, d2, 0, -sp.pi/2],
    [0, d3, 0, 0]
])

# compute A matrices
def compute_A_matrices():
    A_matrix_list = []

    # A matrices for each joint
    for i in range(3):
        q, d, a, alpha = DH[i, :]
        A = sp.Matrix([
            [sp.cos(q), -sp.cos(alpha)*sp.sin(q), sp.sin(alpha)*sp.sin(q), a*sp.cos(q)],
            [sp.sin(q), sp.cos(alpha)*sp.cos(q), -sp.sin(alpha)*sp.cos(q), a*sp.sin(q)],
            [0, sp.sin(alpha), sp.cos(alpha), d],
            [0, 0, 0, 1]
        ])
        A_matrix_list.append(A)
    return A_matrix_list

# compute Jacobian for DH table
def compute_jacobian(A_matrices):
    T = sp.eye(4)
    z = [sp.Matrix([0, 0, 1])]
    o = [sp.Matrix([0, 0, 0])]

    # compute z and o for each joint
    for A in A_matrices:
        T = T * A
        z.append(T[:3, 2])
        o.append(T[:3, 3])

    Jv = []
    Jw = []

    # compute Jv and Jw for each joint, 0 is revolute joint, others prismatic joints
    for i in range(3):
        if i == 0:
            Jv.append(z[i].cross(o[-1] - o[i]))
            Jw.append(z[i])
        else:
            Jv.append(z[i])
            Jw.append(sp.Matrix([0, 0, 0]))
        
    # combine Jv and Jw
    J = sp.Matrix.vstack(sp.Matrix.hstack(*Jv), sp.Matrix.hstack(*Jw))
    return J

# find singularities
def find_singularities(J):
    Jv = J[:3, :]
    Jw = J[3:, :]

    # compute determinant of Jv and Jw
    det_Jv = Jv.det()
    print('Determinant of Jv:', det_Jv)
    det_Jw = Jw.det()
    print('Determinant of Jw:', det_Jw)

    # simplify determinant of Jv and Jw
    det_Jv_simplified = sp.simplify(det_Jv)
    print('Simplified determinant of Jv:', det_Jv_simplified)
    det_Jw_simplified = sp.simplify(det_Jw)
    print('Simplified determinant of Jw:', det_Jw_simplified)

    # find singularities
    sing_Jv = sp.solve(det_Jv_simplified, [q1, d2, d3])
    sing_Jw = sp.solve(det_Jw_simplified, [q1, d2, d3])

    # print singularities
    print("Singularities for Jv (linear part):")
    for sol in sing_Jv:
        print(sol)

    # print singularities
    print("Singularities for Jw (angular part):")
    for sol in sing_Jw:
        print(sol)

    return sing_Jv, sing_Jw

if __name__ == "__main__":
    A_matrices = compute_A_matrices()
    Jacobian = compute_jacobian(A_matrices)
    print("Jacobian Matrix:")
    sp.pprint(Jacobian)
    singularities = find_singularities(Jacobian)
