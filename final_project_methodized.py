from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import time

from gazebo_ros_link_attacher.srv import Attach, AttachRequest, AttachResponse
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from gazebo_msgs.srv import SpawnModel, DeleteModel
from geometry_msgs.msg import Pose


# move from starting singularity 
def move_to_safe(move_group):
    # get current joint values
    joint_goal = move_group.get_current_joint_values()

    # safe joint values
    joint_goal[0] = 0
    joint_goal[1] = -1.5707963267948966
    joint_goal[2] = -1.5707963267948966
    joint_goal[3] = -1.5707963267948966
    joint_goal[4] = 0
    joint_goal[5] = 0 

    # execute movement
    move_group.go(joint_goal, wait=True)

# move to starting position
def move_to_start(move_group, distance=0.1):
    # get current joint values
    joint_goal = move_group.get_current_joint_values()

    # starting joint values for all initials
    joint_goal[0] = 0.1
    joint_goal[1] = -0.87
    joint_goal[2] = -0.75
    joint_goal[3] = -1.57
    joint_goal[4] = -1.57
    joint_goal[5] = 0 

    # execute movement
    move_group.go(joint_goal, wait=True)

# adapted from Jonathan Pilands code on Ed
def move_to_pose(ee_group, location: geometry_msgs.msg._Point.Point, orientation: geometry_msgs.msg._Quaternion.Quaternion):
    # Move to a pose
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.position = location
    pose_goal.orientation = orientation

    ee_group.set_pose_target(pose_goal)
    plan = ee_group.go(wait=True)
    ee_group.stop()
    ee_group.clear_pose_targets()

    return plan

# adapted from Jonathan Pilands code on Ed
def go(self, x = None, y = None, z = None, qx = None, qy = None, qz = None, qw = None):
    # Change one or more of the robot EE parameters
    
    # Grab the last pose if we have not previously done so
    old_pose = self.instance.move_group.get_current_pose().pose
    if self.last_x is None:
        self.last_x = old_pose.position.x
    if self.last_y is None:
        self.last_y = old_pose.position.y
    if self.last_z is None:
        self.last_z = old_pose.position.z
    if self.last_qx is None:
        self.last_qx = old_pose.orientation.x
    if self.last_qy is None:
        self.last_qy = old_pose.orientation.y
    if self.last_qz is None:
        self.last_qz = old_pose.orientation.z
    if self.last_qw is None:
        self.last_qw = old_pose.orientation.w

    # Set parameters if not present
    if x is None:
        x = self.last_x
    if y is None:
        y = self.last_y
    if z is None:
        z = self.last_z
    if qx is None:
        qx = self.last_qx
    if qy is None:
        qy = self.last_qy
    if qz is None:
        qz = self.last_qz
    if qw is None:
        qw = self.last_qw

    # Store updates
    self.last_x = x
    self.last_y = y
    self.last_z = z
    self.last_qx = qx
    self.last_qy = qy
    self.last_qz = qz
    self.last_qw = qw
    move_to_pose(self.instance, geometry_msgs.msg.Point(x, y, z), geometry_msgs.msg.Quaternion(qx, qy, qz, qw))

def cartesian_move(axis, amount, move_group):
    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    if axis == 'x':
        waypoints[0].position.x += amount
    elif axis == 'y':
        waypoints[0].position.y += amount
    elif axis == 'z':
        waypoints[0].position.z += amount
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)
    time.sleep(0.5)

def attach(service, model1, link1, model2, link2):
    log_msg = "Attaching %s:%s to %s:%s" % (model1, link1, model2, link2)
    rospy.loginfo(log_msg)
    req = AttachRequest()
    req.model_name_1 = model1
    req.link_name_1 = link1
    req.model_name_2 = model2
    req.link_name_2 = link2
    service.call(req)

def detach(service, model1, link1, model2, link2):
    log_msg = "Detaching %s:%s from %s:%s" % (model1, link1, model2, link2)
    rospy.loginfo(log_msg)
    req = AttachRequest()
    req.model_name_1 = model1
    req.link_name_1 = link1
    req.model_name_2 = model2
    req.link_name_2 = link2
    service.call(req)

if __name__ == "__main__":
    # initialize moveit_commander and rospy
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

    # instantiate robot commander object
    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()

    # instantiate move group commander object
    group_name = "ur5e_arm"
    move_group = moveit_commander.MoveGroupCommander(group_name)
    ee_group = moveit_commander.MoveGroupCommander("robotiq_hand")

    # create display trajectory publisher
    display_trajectory_publisher = rospy.Publisher(
        "/move_group/display_planned_path",
        moveit_msgs.msg.DisplayTrajectory,
        queue_size=20,
    )

    # Getting planning frame
    planning_frame = move_group.get_planning_frame()
    print("============ Planning frame: %s" % planning_frame)

    # Getting eef link
    eef_link = move_group.get_end_effector_link()
    print("============ End effector link: %s" % eef_link)

    # Getting group names
    group_names = robot.get_group_names()
    print("============ Available Planning Groups:", robot.get_group_names())

    # Getting entire robot state
    print("============ Printing robot state")
    print(robot.get_current_state())
    print("")


    # move to safe position
    move_to_safe(move_group)
    time.sleep(5)
    move_to_safe(move_group)
    time.sleep(2)

    # move to starting position
    move_to_start(move_group)
    time.sleep(5)
    move_to_start(move_group)
    time.sleep(2)

    # move to over grill
    joint_goal = move_group.get_current_joint_values()

    joint_goal[0] = 1.57
    joint_goal[1] = -0.87
    joint_goal[2] = -0.75
    joint_goal[3] = -1.57
    joint_goal[4] = -1.57
    joint_goal[5] = 0 

    move_group.go(joint_goal, wait=True)

    # turn hand to pick up burger
    joint_goal = move_group.get_current_joint_values()

    joint_goal[0] = 1.57
    joint_goal[1] = -0.87
    joint_goal[2] = -0.75
    joint_goal[3] = -1.57
    joint_goal[4] = -1.57
    joint_goal[5] = -1.57 

    move_group.go(joint_goal, wait=True)

    # align with burger
    cartesian_move('z', -0.27, move_group)
    cartesian_move('x', 0.145, move_group)
    ee_group.go([0.0])
    cartesian_move('y', 0.3, move_group)

    # attach to burger
    rospy.loginfo("Creating ServiceProxy to /link_attacher_node/attach")
    attach_srv = rospy.ServiceProxy('/link_attacher_node/attach',
                                    Attach)
    attach_srv.wait_for_service()

    attach(attach_srv, "robot", "wrist_3_link", "burger_cube_small_collision", "link_1")

    # lift up and flip
    cartesian_move('z', 0.2, move_group)
    joint_goal = move_group.get_current_joint_values()
    joint_goal[5] = 1.57 # previous value was -1.57
    move_group.go(joint_goal, wait=True)
    time.sleep(10)
    cartesian_move('z', -0.125, move_group)
    time.sleep(2)


    # set down
    rospy.loginfo("Creating ServiceProxy to /link_attacher_node/detach")
    detach_srv = rospy.ServiceProxy('/link_attacher_node/detach',
                                    Attach)
    detach_srv.wait_for_service()
    rospy.loginfo("Created ServiceProxy to /link_attacher_node/detach")

    detach(detach_srv, "robot", "wrist_3_link", "burger_cube_small_collision", "link_1")

    # cook
    cartesian_move('y', -0.3, move_group)

    print('COLOR CHANGE')

    pose = Pose()
    pose.position.x = 0.012426
    pose.position.y = 0.857568
    pose.position.z = 0.825014

    # service to spawn new burger
    spawn_model_client = rospy.ServiceProxy('/gazebo/spawn_sdf_model', SpawnModel)
    
    # delete old burger
    delete_model_client = rospy.ServiceProxy('/gazebo/delete_model', DeleteModel)
    delete_model_client('burger_cube_small_collision')

    # complete spawn
    spawn_model_client(
        model_name='burger_cube_small_collision_clone',
        model_xml=open('/home/jackson/model_editor_models/burger_cube_small_collision_clone/model.sdf', 'r').read(),
        robot_namespace='/foo',
        initial_pose=pose,
        reference_frame='world'
    ) 

    time.sleep(3)
    print("moving forward")

    # pick up and move to prep station
    cartesian_move('y', 0.3, move_group)
    attach(attach_srv, "robot", "wrist_3_link", "burger_cube_small_collision_clone", "link_1")
    time.sleep(2)


    cartesian_move('z', 0.2, move_group)
    time.sleep(2)
    joint_goal = move_group.get_current_joint_values()
    joint_goal[0] += 1.57
    move_group.go(joint_goal, wait=True)
    cartesian_move('z', -0.14, move_group)
    cartesian_move('x', 0.03, move_group)
    cartesian_move('y', -0.285, move_group)

    # drop burger on bun
    detach(detach_srv, "robot", "wrist_3_link", "burger_cube_small_collision_clone", "link_1")

    # link burger and bun
    attach(attach_srv, "burger_cube_small_collision_clone", "link_1", "buger_cube_bun", "link_1")
    cartesian_move('z', 0.2, move_group)

    # save this joint state
    over_top = move_group.get_current_joint_values()

    # ketchup
    # align with ketchup
    cartesian_move('x', 0.1, move_group)
    cartesian_move('y', 0.49, move_group)
    cartesian_move('z', -0.1, move_group)
    cartesian_move('x', -0.1, move_group)

    # pick it up
    ee_group.go([0.3])
    attach(attach_srv, "robot", "wrist_3_link", "ketchup3", "link_1")

    
    # tilt bottle 
    cartesian_move('z', 0.1, move_group)
    cartesian_move('y', -0.34, move_group)
    joint_goal = move_group.get_current_joint_values()
    joint_goal[5] -= 1.57
    move_group.go(joint_goal, wait=True)
    time.sleep(5)
    joint_goal = move_group.get_current_joint_values()
    joint_goal[5] += 1.57
    move_group.go(joint_goal, wait=True)
    time.sleep(5)
    cartesian_move('y', 0.34, move_group)
    cartesian_move('z', -0.05, move_group)
    ee_group.go([0.0])

    # drop ketchup
    detach(detach_srv, "robot", "wrist_3_link", "ketchup3", "link_1")

    # move to top of burger
    cartesian_move('x', 0.1, move_group)
    cartesian_move('z', 0.05, move_group)
    cartesian_move('y', -0.49, move_group)
    move_group.go(over_top, wait=True)
    # end ketchup

    # move to top bun
    cartesian_move('y', 0.3, move_group)
    cartesian_move('z', -0.14, move_group)

    # attach top bun
    attach(attach_srv, "robot", "wrist_3_link", "burger_cube_bun_top", "link_1")

    # align with top of burger
    cartesian_move('x', 0.2, move_group)
    cartesian_move('z', 0.14, move_group)
    cartesian_move('y', -0.32, move_group)
    cartesian_move('x', -0.3, move_group)

    # drop top bun
    detach(detach_srv, "robot", "wrist_3_link", "burger_cube_bun_top", "link_1")

    # link top bun and burger
    attach(attach_srv, "burger_cube_small_collision_clone", "link_1", "buger_cube_bun_top", "link_1")

    # move away from burger
    cartesian_move('x', 0.3, move_group)