from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import time

from gazebo_ros_link_attacher.srv import Attach, AttachRequest, AttachResponse

# tutorial
try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# move from starting singularity 
def move_to_safe(move_group):
    # get current joint values
    joint_goal = move_group.get_current_joint_values()

    # safe joint values
    joint_goal[0] = 0
    joint_goal[1] = -1.5707963267948966
    joint_goal[2] = -1.5707963267948966
    joint_goal[3] = -1.5707963267948966
    joint_goal[4] = 0
    joint_goal[5] = 0 

    # execute movement
    move_group.go(joint_goal, wait=True)

# move to starting position to draw J,T, and P
def move_to_start(move_group, distance=0.1):
    # get current joint values
    joint_goal = move_group.get_current_joint_values()

    # starting joint values for all initials
    joint_goal[0] = 0.1
    joint_goal[1] = -0.87
    joint_goal[2] = -0.75
    joint_goal[3] = -1.57
    joint_goal[4] = -1.57
    joint_goal[5] = 0 

    # execute movement
    move_group.go(joint_goal, wait=True)


def move_to_pose(ee_group, location: geometry_msgs.msg._Point.Point, orientation: geometry_msgs.msg._Quaternion.Quaternion):
    # Move to a pose
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.position = location
    pose_goal.orientation = orientation

    ee_group.set_pose_target(pose_goal)
    plan = ee_group.go(wait=True)
    ee_group.stop()
    ee_group.clear_pose_targets()

    return plan

def go(self, x = None, y = None, z = None, qx = None, qy = None, qz = None, qw = None):
    # Change one or more of the robot EE parameters
    
    # Grab the last pose if we have not previously done so
    old_pose = self.instance.move_group.get_current_pose().pose
    if self.last_x is None:
        self.last_x = old_pose.position.x
    if self.last_y is None:
        self.last_y = old_pose.position.y
    if self.last_z is None:
        self.last_z = old_pose.position.z
    if self.last_qx is None:
        self.last_qx = old_pose.orientation.x
    if self.last_qy is None:
        self.last_qy = old_pose.orientation.y
    if self.last_qz is None:
        self.last_qz = old_pose.orientation.z
    if self.last_qw is None:
        self.last_qw = old_pose.orientation.w

    # Set parameters if not present
    if x is None:
        x = self.last_x
    if y is None:
        y = self.last_y
    if z is None:
        z = self.last_z
    if qx is None:
        qx = self.last_qx
    if qy is None:
        qy = self.last_qy
    if qz is None:
        qz = self.last_qz
    if qw is None:
        qw = self.last_qw

    # Store updates
    self.last_x = x
    self.last_y = y
    self.last_z = z
    self.last_qx = qx
    self.last_qy = qy
    self.last_qz = qz
    self.last_qw = qw
    move_to_pose(self.instance, geometry_msgs.msg.Point(x, y, z), geometry_msgs.msg.Quaternion(qx, qy, qz, qw))


if __name__ == "__main__":
    # initialize moveit_commander and rospy
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

    # instantiate robot commander object
    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()

    # instantiate move group commander object
    group_name = "ur5e_arm"
    move_group = moveit_commander.MoveGroupCommander(group_name)
    ee_group = moveit_commander.MoveGroupCommander("robotiq_hand")

    # create display trajectory publisher
    display_trajectory_publisher = rospy.Publisher(
        "/move_group/display_planned_path",
        moveit_msgs.msg.DisplayTrajectory,
        queue_size=20,
    )

    # Getting planning frame
    planning_frame = move_group.get_planning_frame()
    print("============ Planning frame: %s" % planning_frame)

    # Getting eef link
    eef_link = move_group.get_end_effector_link()
    print("============ End effector link: %s" % eef_link)

    # Getting group names
    group_names = robot.get_group_names()
    print("============ Available Planning Groups:", robot.get_group_names())

    # Getting entire robot state
    print("============ Printing robot state")
    print(robot.get_current_state())
    print("")


    # move to safe position
    move_to_safe(move_group)
    time.sleep(5)
    move_to_safe(move_group)
    time.sleep(2)

    # move to starting position
    move_to_start(move_group)
    time.sleep(5)
    move_to_start(move_group)
    time.sleep(2)

    # get current joint values
    joint_goal = move_group.get_current_joint_values()

    # starting joint values for all initials
    joint_goal[0] = 1.57
    joint_goal[1] = -0.87
    joint_goal[2] = -0.75
    joint_goal[3] = -1.57
    joint_goal[4] = -1.57
    joint_goal[5] = 0 

    # time.sleep(2)

    # execute movement
    move_group.go(joint_goal, wait=True)

            # get current joint values
    joint_goal = move_group.get_current_joint_values()

    # starting joint values for all initials
    joint_goal[0] = 1.57
    joint_goal[1] = -0.87
    joint_goal[2] = -0.75
    joint_goal[3] = -1.57
    joint_goal[4] = -1.57
    joint_goal[5] = -1.57 

    # time.sleep(2)

    # execute movement
    move_group.go(joint_goal, wait=True)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z -= 0.27 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    time.sleep(2)
    move_group.execute(plan, wait=True)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.x += 0.145
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    time.sleep(2)
    move_group.execute(plan, wait=True)

    ee_group.go([0.0])
    # time.sleep(2)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y += 0.3 # move forward in y direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    time.sleep(2)
    move_group.execute(plan, wait=True)

    # # ee_group.go([0.0])
    # # time.sleep(2)

    # # current_pose = move_group.get_current_pose().pose
    # # waypoints = [copy.deepcopy(current_pose)]
    # # waypoints[0].position.z -= 0.1 # move down in z direction
    # # (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # # time.sleep(2)
    # # move_group.execute(plan, wait=True)

    # ee_group.go([0.1])
    # time.sleep(2)

    # # run this rosservice call /link_attacher_node/attach "model_name_1: 'robot'
    # # link_name_1: 'wrist_3_link'
    # # model_name_2: 'unit_box'
    # # link_name_2: 'link'"

    # time.sleep(2)

    rospy.loginfo("Creating ServiceProxy to /link_attacher_node/attach")
    attach_srv = rospy.ServiceProxy('/link_attacher_node/attach',
                                    Attach)
    attach_srv.wait_for_service()
    rospy.loginfo("Created ServiceProxy to /link_attacher_node/attach")

    # Link them
    rospy.loginfo("Attaching end effector and burger")
    req = AttachRequest()
    req.model_name_1 = "robot"
    req.link_name_1 = "wrist_3_link"
    req.model_name_2 = "burger_cube_small_collision"
    req.link_name_2 = "link_1"

    attach_srv.call(req)

    # time.sleep(2)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z += 0.2 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    joint_goal = move_group.get_current_joint_values()

    joint_goal[5] = 1.57

    # time.sleep(2)

    move_group.go(joint_goal, wait=True)

    time.sleep(10)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z -= 0.125 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)


    rospy.loginfo("Creating ServiceProxy to /link_attacher_node/detach")
    attach_srv = rospy.ServiceProxy('/link_attacher_node/detach',
                                    Attach)
    attach_srv.wait_for_service()
    rospy.loginfo("Created ServiceProxy to /link_attacher_node/detach")

    # Link them
    rospy.loginfo("detaching end effector and burger")
    req = AttachRequest()
    req.model_name_1 = "robot"
    req.link_name_1 = "wrist_3_link"
    req.model_name_2 = "burger_cube_small_collision"
    req.link_name_2 = "link_1"

    attach_srv.call(req)

    print("moving back")

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y -= 0.3 # move forward in y direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    time.sleep(2)
    move_group.execute(plan, wait=True)

    # change color of burger_cube_small_collision

    time.sleep(3)

    print("moving forward")

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y += 0.3 # move forward in y direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    time.sleep(2)
    move_group.execute(plan, wait=True)

    # current_pose = move_group.get_current_pose().pose
    # waypoints = [copy.deepcopy(current_pose)]
    # waypoints[0].position.z -= 0.05 # move forward in y direction
    # (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    # move_group.execute(plan, wait=True)

    # time.sleep(2)

    rospy.loginfo("Creating ServiceProxy to /link_attacher_node/attach")
    attach_srv = rospy.ServiceProxy('/link_attacher_node/attach',
                                    Attach)
    attach_srv.wait_for_service()
    rospy.loginfo("Created ServiceProxy to /link_attacher_node/attach")

    # Link them
    rospy.loginfo("Attaching end effector and burger")
    req = AttachRequest()
    req.model_name_1 = "robot"
    req.link_name_1 = "wrist_3_link"
    req.model_name_2 = "burger_cube_small_collision"
    req.link_name_2 = "link_1"

    attach_srv.call(req)

    time.sleep(2)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z += 0.2 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    joint_goal = move_group.get_current_joint_values()

    joint_goal[0] += 1.57

    # time.sleep(2)

    move_group.go(joint_goal, wait=True)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z -= 0.14 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.x += 0.03 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y -= 0.285 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)


    rospy.loginfo("Creating ServiceProxy to /link_attacher_node/detach")
    attach_srv = rospy.ServiceProxy('/link_attacher_node/detach',
                                    Attach)
    attach_srv.wait_for_service()
    rospy.loginfo("Created ServiceProxy to /link_attacher_node/detach")

    # Link them
    rospy.loginfo("detaching end effector and burger")
    req = AttachRequest()
    req.model_name_1 = "robot"
    req.link_name_1 = "wrist_3_link"
    req.model_name_2 = "burger_cube_small_collision"
    req.link_name_2 = "link_1"

    attach_srv.call(req)

    rospy.loginfo("Creating ServiceProxy to /link_attacher_node/attach")
    attach_srv = rospy.ServiceProxy('/link_attacher_node/detach',
                                    Attach)
    attach_srv.wait_for_service()
    rospy.loginfo("Created ServiceProxy to /link_attacher_node/detach")

    # Link them
    rospy.loginfo("attaching end effector and burger")
    req = AttachRequest()
    req.model_name_1 = "burger_cube_small_collision"
    req.link_name_1 = "link_1"
    req.model_name_2 = "buger_cube_bun"
    req.link_name_2 = "link_1"

    attach_srv.call(req)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z += 0.2 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    over_top = move_group.get_current_joint_values()
    # move_group.go(over_top, wait=True)


    # ketchup

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y += 0.49 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    # current_pose = move_group.get_current_pose().pose
    # waypoints = [copy.deepcopy(current_pose)]
    # waypoints[0].position.x -= 0.3 # move down in z direction
    # (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # # time.sleep(2)
    # move_group.execute(plan, wait=True)
    # time.sleep(2)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z -= 0.1 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    ee_group.go([0.3])

    rospy.loginfo("Creating ServiceProxy to /link_attacher_node/attach")
    attach_srv = rospy.ServiceProxy('/link_attacher_node/attach',
                                    Attach)
    attach_srv.wait_for_service()
    rospy.loginfo("Created ServiceProxy to /link_attacher_node/attach")

    # Link them
    rospy.loginfo("Attaching end effector and burger")
    req = AttachRequest()
    req.model_name_1 = "robot"
    req.link_name_1 = "wrist_3_link"
    req.model_name_2 = "ketchup3"
    req.link_name_2 = "link_1"

    attach_srv.call(req)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z += 0.1 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)


    # current_pose = move_group.get_current_pose().pose
    # waypoints = [copy.deepcopy(current_pose)]
    # waypoints[0].position.x += 0.3 # move down in z direction
    # (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # # time.sleep(2)
    # move_group.execute(plan, wait=True)
    # time.sleep(2)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y -= 0.34 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    joint_goal = move_group.get_current_joint_values()

    joint_goal[5] -= 1.57

    move_group.go(joint_goal, wait=True)

    time.sleep(5)


    joint_goal = move_group.get_current_joint_values()

    joint_goal[5] += 1.57

    move_group.go(joint_goal, wait=True)

    time.sleep(5)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y += 0.34 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    # current_pose = move_group.get_current_pose().pose
    # waypoints = [copy.deepcopy(current_pose)]
    # waypoints[0].position.x -= 0.3 # move down in z direction
    # (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # # time.sleep(2)
    # move_group.execute(plan, wait=True)
    # time.sleep(2)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z -= 0.05 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    ee_group.go([0.0])

    rospy.loginfo("Creating ServiceProxy to /link_attacher_node/detach")
    attach_srv = rospy.ServiceProxy('/link_attacher_node/detach',
                                    Attach)
    attach_srv.wait_for_service()
    rospy.loginfo("Created ServiceProxy to /link_attacher_node/attach")

    # Link them
    rospy.loginfo("detaching end effector and burger")
    req = AttachRequest()
    req.model_name_1 = "robot"
    req.link_name_1 = "wrist_3_link"
    req.model_name_2 = "ketchup3"
    req.link_name_2 = "link_1"

    attach_srv.call(req)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z += 0.05 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)


    # current_pose = move_group.get_current_pose().pose
    # waypoints = [copy.deepcopy(current_pose)]
    # waypoints[0].position.x += 0.3 # move down in z direction
    # (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # # time.sleep(2)
    # move_group.execute(plan, wait=True)
    # time.sleep(2)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y -= 0.49 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    # end ketchup

    move_group.go(over_top, wait=True)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y += 0.30 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z -= 0.14 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    rospy.loginfo("Creating ServiceProxy to /link_attacher_node/attach")
    attach_srv = rospy.ServiceProxy('/link_attacher_node/attach',
                                    Attach)
    attach_srv.wait_for_service()
    rospy.loginfo("Created ServiceProxy to /link_attacher_node/attach")

    # Link them
    rospy.loginfo("Attaching end effector and burger")
    req = AttachRequest()
    req.model_name_1 = "robot"
    req.link_name_1 = "wrist_3_link"
    req.model_name_2 = "burger_cube_bun_top"
    req.link_name_2 = "link_1"

    attach_srv.call(req)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.x += 0.2 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z += 0.14 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y -= 0.32 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.x -= 0.3 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)

    # move_group.go(over_top, wait=True)

    rospy.loginfo("Creating ServiceProxy to /link_attacher_node/detach")
    attach_srv = rospy.ServiceProxy('/link_attacher_node/detach',
                                    Attach)
    attach_srv.wait_for_service()
    rospy.loginfo("Created ServiceProxy to /link_attacher_node/detach")

    # Link them
    rospy.loginfo("detaching end effector and bun")
    req = AttachRequest()
    req.model_name_1 = "robot"
    req.link_name_1 = "wrist_3_link"
    req.model_name_2 = "burger_cube_bun_top"
    req.link_name_2 = "link_1"

    attach_srv.call(req)

    rospy.loginfo("Creating ServiceProxy to /link_attacher_node/attach")
    attach_srv = rospy.ServiceProxy('/link_attacher_node/detach',
                                    Attach)
    attach_srv.wait_for_service()
    rospy.loginfo("Created ServiceProxy to /link_attacher_node/detach")

    # Link them
    rospy.loginfo("attaching end bun and burger")
    req = AttachRequest()
    req.model_name_1 = "burger_cube_small_collision"
    req.link_name_1 = "link_1"
    req.model_name_2 = "buger_cube_bun"
    req.link_name_2 = "link_1"

    attach_srv.call(req)

    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.x += 0.2 # move down in z direction
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    # time.sleep(2)
    move_group.execute(plan, wait=True)
    time.sleep(2)