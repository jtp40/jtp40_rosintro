#!/usr/bin/bash

rosservice call /turtle1/set_pen 255 0 0 2 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[-4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, -3.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 0.0, 0.0]' '[0.0, 0.0, -2.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2.0, 0.0, 0.0]' '[0.0, 0.0, -2.0]'

rosservice call /spawn 5 2.5 0.0 'turtle2'
rosservice call /turtle2/set_pen 0 255 0 2 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[0.0 , 13.0 , 0.0]' '[0.0 , 0.0 , -12.0]'
rosservice call /turtle2/set_pen 0 0 0 2 1
rosservice call /turtle2/teleport_absolute 7.15 2.25 0.0
rosservice call /turtle2/set_pen 0 255 0 2 0
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[0.0 , -0.9 , 0.0]' '[0.0 , 0.0 , 0.0]'
rosservice call /turtle2/set_pen 0 0 0 2 1
rosservice call /turtle2/teleport_absolute 8.15 2.25 0.0



