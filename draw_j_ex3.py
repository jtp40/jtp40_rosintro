from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import time


# tutorial
try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# move from starting singularity 
def move_to_safe(move_group):
    # get current joint values
    joint_goal = move_group.get_current_joint_values()

    # safe joint values
    joint_goal[0] = 0
    joint_goal[1] = -1.5707963267948966
    joint_goal[2] = -1.5707963267948966
    joint_goal[3] = -1.5707963267948966
    joint_goal[4] = 0
    joint_goal[5] = 0 

    move_group.go(joint_goal, wait=True)

# move to starting position to draw J
def move_to_start(move_group, distance=0.1):
    # get current joint values
    joint_goal = move_group.get_current_joint_values()

    # starting joint values for J
    joint_goal[0] = 0.1
    joint_goal[1] = -0.87
    joint_goal[2] = -0.75
    joint_goal[3] = -1.57
    joint_goal[4] = -1.57
    joint_goal[5] = 0 

    move_group.go(joint_goal, wait=True)

# draw J that looks like this:
# ___________________________
#             |
#             |
#             |
#             |
#             |
#             |
#             |
#             |
#             |
#             |
# ____________|
def draw_j(move_group):
    # draw half of top horizontal line of capital J, starting from top middle and going to top right
    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y += 0.2
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)

    # go in reverse direction to draw other half of top horizontal line of capital J, starting at top right and going to top left
    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y -= 0.4
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)

    # return to top middle of capital J
    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y += 0.2
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)

    # draw vertical line of capital J, starting from top middle and going to bottom middle
    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z -= 0.3
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)

    # draw bottom horizontal line of capital J, starting from bottom middle and going to bottom left
    current_pose = move_group.get_current_pose().pose
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y -= 0.2
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)

if __name__ == "__main__":
    # initialize moveit_commander and rospy
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

    # instantiate robot commander object
    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()

    # instantiate move group commander object
    group_name = "manipulator"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    # create display trajectory publisher
    display_trajectory_publisher = rospy.Publisher(
        "/move_group/display_planned_path",
        moveit_msgs.msg.DisplayTrajectory,
        queue_size=20,
    )

    # Getting planning frame
    planning_frame = move_group.get_planning_frame()
    print("============ Planning frame: %s" % planning_frame)

    # Getting eef link
    eef_link = move_group.get_end_effector_link()
    print("============ End effector link: %s" % eef_link)

    # Getting group names
    group_names = robot.get_group_names()
    print("============ Available Planning Groups:", robot.get_group_names())

    # Getting entire robot state
    print("============ Printing robot state")
    print(robot.get_current_state())
    print("")


    move_to_safe(move_group)

    move_to_start(move_group)

    print("Drawing J")
    draw_j(move_group)
    print("Completed J")

    time.sleep(2)

    print("Moving back to start")
    move_to_start(move_group)