from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import time


# tutorial
try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# move from starting singularity 
def move_to_safe(move_group):
    # get current joint values
    joint_goal = move_group.get_current_joint_values()

    # safe joint values
    joint_goal[0] = 0
    joint_goal[1] = -1.5707963267948966
    joint_goal[2] = -1.5707963267948966
    joint_goal[3] = -1.5707963267948966
    joint_goal[4] = 0
    joint_goal[5] = 0 

    move_group.go(joint_goal, wait=True)

# move to starting position to draw J
def move_to_start(move_group, distance=0.1):
    # get current joint values
    joint_goal = move_group.get_current_joint_values()

    # starting joint values for J
    joint_goal[0] = 0.1
    joint_goal[1] = -0.87
    joint_goal[2] = -0.75
    joint_goal[3] = -1.57
    joint_goal[4] = -1.57
    joint_goal[5] = 0 

    move_group.go(joint_goal, wait=True)

# move to starting position to draw J
def move_to_start_pose(move_group, distance=0.1):
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.position.x = 0.3219915373342611
    pose_goal.position.y = 0.1419722648424742
    pose_goal.position.z = 0.9034457341602529

    pose_goal.orientation.x= -0.5125205641417122
    pose_goal.orientation.y = 0.4634269892079982
    pose_goal.orientation.z = -0.48486141835556806
    pose_goal.orientation.w = 0.5361599593364819

    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)
    move_group.stop()
    move_group.clear_pose_targets()

# draw J that looks like this:
# ___________________________
#             |
#             |
#             |
#             |
#             |
#             |
#             |
#             |
#             |
#             |
# ____________|
def draw_j_cartesian(move_group):
    # draw half of top horizontal line of capital J, starting from top middle and going to top right
    current_pose = move_group.get_current_pose().pose
    print("Pose 1", current_pose)
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y += 0.2
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)

    # go in reverse direction to draw other half of top horizontal line of capital J, starting at top right and going to top left
    current_pose = move_group.get_current_pose().pose
    print("Pose 2", current_pose)
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y -= 0.4
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)

    # return to top middle of capital J
    current_pose = move_group.get_current_pose().pose
    print("Pose 3", current_pose)
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y += 0.2
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)

    # draw vertical line of capital J, starting from top middle and going to bottom middle
    current_pose = move_group.get_current_pose().pose
    print("Pose 4", current_pose)
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.z -= 0.3
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)

    # draw bottom horizontal line of capital J, starting from bottom middle and going to bottom left
    current_pose = move_group.get_current_pose().pose
    print("Pose 5", current_pose)
    waypoints = [copy.deepcopy(current_pose)]
    waypoints[0].position.y -= 0.2
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)

def draw_j_pose(move_group):
    # drawing J using pose goals, pose goals retrieved from the previous cartesian path

    # draw half of top horizontal line of capital J, starting from top middle and going to top right
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.position.x = 0.3218776774753912
    pose_goal.position.y = 0.334655567737481
    pose_goal.position.z = 0.9034457341602529

    pose_goal.orientation.x= -0.5125205641417122
    pose_goal.orientation.y = 0.4634269892079982
    pose_goal.orientation.z = -0.48486141835556806
    pose_goal.orientation.w = 0.5361599593364819

    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)
    move_group.stop()
    move_group.clear_pose_targets()

    # go in reverse direction to draw other half of top horizontal line of capital J, starting at top right and going to top left
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.position.x = 0.3218776774753912
    pose_goal.position.y = -0.06654158440057566
    pose_goal.position.z = 0.9034457341602529

    pose_goal.orientation.x= -0.5125205641417122
    pose_goal.orientation.y = 0.4634269892079982
    pose_goal.orientation.z = -0.48486141835556806
    pose_goal.orientation.w = 0.5361599593364819

    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)
    move_group.stop()
    move_group.clear_pose_targets()

    # return to top middle of capital J
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.position.x = 0.3218776774753912
    pose_goal.position.y = 0.1335155739442086
    pose_goal.position.z = 0.9034457341602529

    pose_goal.orientation.x= -0.5125205641417122
    pose_goal.orientation.y = 0.4634269892079982
    pose_goal.orientation.z = -0.48486141835556806
    pose_goal.orientation.w = 0.5361599593364819

    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)
    move_group.stop()
    move_group.clear_pose_targets()

    # draw vertical line of capital J, starting from top middle and going to bottom middle
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.position.x = 0.3218776774753912
    pose_goal.position.y = 0.1335155739442086
    pose_goal.position.z = 0.5986945089675894

    pose_goal.orientation.x= -0.5125205641417122
    pose_goal.orientation.y = 0.4634269892079982
    pose_goal.orientation.z = -0.48486141835556806
    pose_goal.orientation.w = 0.5361599593364819

    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)
    move_group.stop()
    move_group.clear_pose_targets()

    # draw bottom horizontal line of capital J, starting from bottom middle and going to bottom left
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.position.x = 0.3218776774753912
    pose_goal.position.y = -0.06654158440057566
    pose_goal.position.z = 0.5986945089675894

    pose_goal.orientation.x= -0.5125205641417122
    pose_goal.orientation.y = 0.4634269892079982
    pose_goal.orientation.z = -0.48486141835556806
    pose_goal.orientation.w = 0.5361599593364819

    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)
    move_group.stop()
    move_group.clear_pose_targets()


if __name__ == "__main__":
    # initialize moveit_commander and rospy
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

    # instantiate robot commander object
    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()

    # instantiate move group commander object
    group_name = "manipulator"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    # create display trajectory publisher
    display_trajectory_publisher = rospy.Publisher(
        "/move_group/display_planned_path",
        moveit_msgs.msg.DisplayTrajectory,
        queue_size=20,
    )

    # Getting planning frame
    planning_frame = move_group.get_planning_frame()
    print("============ Planning frame: %s" % planning_frame)

    # Getting eef link
    eef_link = move_group.get_end_effector_link()
    print("============ End effector link: %s" % eef_link)

    # Getting group names
    group_names = robot.get_group_names()
    print("============ Available Planning Groups:", robot.get_group_names())

    # Getting entire robot state
    print("============ Printing robot state")
    print(robot.get_current_state())
    print("")


    move_to_safe(move_group)

    move_to_start(move_group)
    print(move_group.get_current_pose().pose)

    print("Drawing J cartesian")
    draw_j_cartesian(move_group)
    print("Completed J cartesian")

    time.sleep(2)

    print("Moving back to start cartesian")
    move_to_start(move_group)

    print("Drawing J pose")
    draw_j_pose(move_group)
    print("Completed J pose")

    time.sleep(2)

    print("Moving back to start pose")
    move_to_start_pose(move_group)