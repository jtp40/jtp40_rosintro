#!/usr/bin/env python3

import rospy
from std_msgs.msg import String

def callback(data):
    rospy.loginfo("TA: Received homework. Sending to autograder...")
    pub.publish(data.data)

def ta_node():
    global pub
    rospy.init_node('ta_node')
    pub = rospy.Publisher('autograder_topic', String, queue_size=10)
    rospy.Subscriber('homework_topic', String, callback)
    rospy.spin()

if __name__ == '__main__':
    ta_node()
