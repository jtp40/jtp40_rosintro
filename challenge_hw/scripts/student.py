#!/usr/bin/env python3

import rospy
from std_msgs.msg import String

def feedback_callback(data):
    rospy.loginfo("Student: Received feedback. My grade is: %s", data.data)

def student_node():
    rospy.init_node('student_node')
    pub = rospy.Publisher('homework_topic', String, queue_size=10)
    rospy.Subscriber('feedback_topic', String, feedback_callback)
    
    rate = rospy.Rate(0.2)

    while not rospy.is_shutdown():
        code = "this is my homework"
        rospy.loginfo("Student: Submitting homework to TA...")
        pub.publish(code)
        rate.sleep()

if __name__ == '__main__':
    student_node()
