#!/usr/bin/env python3

import rospy
from std_msgs.msg import String

def callback(data):
    rospy.loginfo(f"Gradebook: Recorded - {data.data}")

def gradebook_node():
    rospy.init_node('gradebook_node')
    rospy.Subscriber('gradebook_topic', String, callback)
    rospy.spin()

if __name__ == '__main__':
    gradebook_node()
