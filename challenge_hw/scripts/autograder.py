#!/usr/bin/env python3

import rospy
import random
from std_msgs.msg import String

def grade_homework(data):
    grade = random.randint(0, 100)
    rospy.loginfo(f"Autograder: Graded homework. Grade: {grade}%")
    pub_gradebook.publish(f"Student's Homework Grade: {grade}%")
    pub_student_feedback.publish(f"{grade}%")

def autograder_node():
    global pub_gradebook, pub_student_feedback

    rospy.init_node('autograder_node')
    pub_gradebook = rospy.Publisher('gradebook_topic', String, queue_size=10)
    pub_student_feedback = rospy.Publisher('feedback_topic', String, queue_size=10)
    rospy.Subscriber('autograder_topic', String, grade_homework)
    
    rospy.spin()

if __name__ == '__main__':
    autograder_node()
